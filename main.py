import requests
from bs4 import BeautifulSoup
import mysql.connector
from sklearn import tree
from sklearn.preprocessing import LabelEncoder

cnx = mysql.connector.connect(user='root', password='@a6217A@',
                              host='127.0.0.1',
                              database='car')
cursor = cnx.cursor()
cursor.execute('''CREATE TABLE IF NOT EXISTS carTable2 (
               model text,
               price text,
               mileage text)''')
yearList = []
priceList = []
mileageList = []

for page in range(1, 201):
    webPage = requests.get(f'https://www.truecar.com/used-cars-for-sale/listings/?page={page}')
    soup = BeautifulSoup(webPage.text, 'html.parser')
    price = soup.find_all('div', attrs={'data-test': 'vehicleCardPricingBlockPrice'})
    year = soup.find_all('div', attrs={'data-test': 'vehicleCardYearMakeModel'})
    mileage = soup.find_all('div', attrs={'data-test': 'vehicleMileage'})

    for year in year:
        yearList.append(year.text)

    for price in price:
        priceList.append(price.text)

    for mileage in mileage:
        mileageList.append(mileage.text)

count = 0

x = []
y = []
for num in yearList:
    x.append([yearList[count], mileageList[count]])

    if count < len(priceList):

        sql = "INSERT INTO carTable2 (model, price, mileage) VALUES (%s, %s, %s)"
        val = (yearList[count], priceList[count], mileageList[count])
        cursor.execute(sql, val)
        cnx.commit()
        y.append(priceList[count])
    else:

        sql = "INSERT INTO carTable2 (model, price, mileage) VALUES (%s, %s, %s)"
        val = (yearList[count], 'No Price', mileageList[count])
        cursor.execute(sql, val)
        cnx.commit()
        y.append('No Price')
    count += 1

le = LabelEncoder()
ML_OR_END = input('Do you want me to price the car? y/n : ')
if ML_OR_END == 'y':
    date = input('input model and mileage : 2018 BMW i3-28,740 miles \n').split('-')
    le.fit(date)
    LabelEncoder()
    list(le.classes_)
    date = le.transform(date)

    lstX = list()
    for x1, x2 in x:
        lstX.append(x1)
        lstX.append(x2)

    le.fit(lstX)
    LabelEncoder()
    list(le.classes_)
    lstX = le.transform(lstX)

    xx = list()
    count = 0
    for num in lstX:
        temp = [lstX[count], lstX[count+1]]
        xx.append(temp)
        count += 1
        if count == len(lstX)/2:
            break

    le.fit(y)
    LabelEncoder()
    list(le.classes_)
    y = le.transform(y)

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(xx, list(y))
    answer = clf.predict([date])
    answer = list(le.inverse_transform(answer))
    print(answer[0])
cnx.close()
